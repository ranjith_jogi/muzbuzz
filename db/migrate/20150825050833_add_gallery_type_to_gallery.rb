class AddGalleryTypeToGallery < ActiveRecord::Migration
  def change
    add_column :galleries, :gallery_type, :string
  end
end
