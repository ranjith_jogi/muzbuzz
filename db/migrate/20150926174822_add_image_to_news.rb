class AddImageToNews < ActiveRecord::Migration
  def up
    add_attachment :news, :image
  end

  def down
    add_attachment :news, :image
  end
end
