class AddCategoryIdToGossipAndBiography < ActiveRecord::Migration
  def change
  	add_column :gossips, :category_id, :integer
  	add_column :biographies, :category_id, :integer
  end
end
