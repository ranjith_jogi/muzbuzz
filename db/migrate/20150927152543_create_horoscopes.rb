class CreateHoroscopes < ActiveRecord::Migration
  def change
    create_table :horoscopes do |t|
    	t.string :name
    	t.text :description
    	t.string :author
    	t.attachment :image
    	t.attachment :video
    	t.string :video_file_name
		  t.string :video_content_type
		  t.integer :video_file_size
		  t.datetime :video_updated_at
		  t.integer :category_id 
      t.timestamps null: false
    end
  end
end
