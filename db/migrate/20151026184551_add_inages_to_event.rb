class AddInagesToEvent < ActiveRecord::Migration
  def change
  	remove_column :events, :guest_image
  	remove_column :events, :host_image

  	add_attachment :events, :guest_image
    add_attachment :events, :host_image

  end
end
