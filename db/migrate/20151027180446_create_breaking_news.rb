class CreateBreakingNews < ActiveRecord::Migration
  def change
    create_table :breaking_news do |t|

      t.timestamps null: false
    end
  end
end
