class CreateUpcomingNews < ActiveRecord::Migration
  def change
    create_table :upcoming_news do |t|

      t.timestamps null: false
    end
  end
end
