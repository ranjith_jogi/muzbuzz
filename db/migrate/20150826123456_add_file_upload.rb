class AddFileUpload < ActiveRecord::Migration
    def up
      add_attachment :galleries, :image
    end

    def down
      add_attachment :galleries, :image
    end
end
