class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.text :description
      t.string :event_guest
      t.string :event_host
      t.date :event_date
      t.string :event_address
      t.string :city
      t.string :state
      t.string :country
      t.string :guest_image
      t.string :host_image

      t.timestamps null: false
    end
  end
end
