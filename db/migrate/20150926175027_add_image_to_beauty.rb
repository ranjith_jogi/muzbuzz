class AddImageToBeauty < ActiveRecord::Migration
  def up
    add_attachment :beauty_and_sexes, :image
  end

  def down
    add_attachment :beauty_and_sexes, :image
  end
end
