class CreateGalleries < ActiveRecord::Migration
  def change
    create_table :galleries do |t|
      t.string :heading
      t.text :description
      t.string :author

      t.timestamps null: false
    end
  end
end
