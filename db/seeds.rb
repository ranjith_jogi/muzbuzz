# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')
Category.create(name: "Beauty&Sex",description: "Beauty&Sex Description")
Category.create(name: "Biography",description: "Biography Description")
Category.create(name: "Cartoon", description: "Cartoon  Description" )
Category.create(name: "Gallery", description: "Gallery  Description" )
Category.create(name: "Gossip", description: "Gossip  Description" )
Category.create(name: "Horoscope", description: "Horoscope  Description" )
Category.create(name: "Cartoon", description: "Cartoon  Description" )
Category.create(name: "Music&Video", description: "Music&Video  Description" )
Category.create(name: "News", description: "News Description" )




