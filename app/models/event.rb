class Event < ActiveRecord::Base

	has_attached_file :guest_image,
    styles: { medium: "1000x1000>", thumb: "1000x1000>" },
    default_url: "/images/:style/missing.png"
  validates_attachment_content_type :guest_image,
    content_type: /\Aimage\/.*\Z/

  has_attached_file :host_image,
	  styles: { medium: "1000x1000>", thumb: "1000x1000>" },
	  default_url: "/images/:style/missing.png"
  validates_attachment_content_type :host_image,
    content_type: /\Aimage\/.*\Z/

end
