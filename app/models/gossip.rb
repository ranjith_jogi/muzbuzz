class Gossip < ActiveRecord::Base

	has_attached_file :image,
    styles: { medium: "1000x1000>", thumb: "1000x1000>" },
    default_url: "/images/:style/missing.png"

  validates_attachment_content_type :image,
    content_type: /\Aimage\/.*\Z/
  has_attached_file :video#, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  validates_attachment_content_type :video, :content_type => ["video/mp4", "video/mov","image/jpg", "image/jpeg", "image/png", "image/gif"]

end
