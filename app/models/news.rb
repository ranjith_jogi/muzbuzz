class News < ActiveRecord::Base
	belongs_to :category 
	self.inheritance_column = :news_type

	has_attached_file :image,
    styles: { medium: "1000x1000>", thumb: "1000x1000>" },
    default_url: "/images/:style/missing.png"

  validates_attachment_content_type :image,
    content_type: /\Aimage\/.*\Z/
  has_attached_file :video#, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  validates_attachment_content_type :video, :content_type => ["video/mp4", "video/mov","image/jpg", "image/jpeg", "image/png", "image/gif"]

	scope :local_news, -> { where(news_type: 'LocalNews') } 
	scope :national_news, -> { where(news_type: 'NationalNews') } 
	scope :international_news, -> { where(news_type: 'InternationalNews') }

	def self.news_type
    %w(LocalNews NationalNews InternationalNews GeneralNews MiscelleneousNews UpcomingNews TrendingNews BreakingNews)
  end

  def self.search(query)
	  where("heading ILIKE ?", "%#{query}%") 
	  where("description ILIKE ?", "%#{query}%")
	end
end
