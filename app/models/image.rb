class Image < ActiveRecord::Base
	belongs_to :music_and_video
	has_attached_file :image,
    styles: { medium: "1000x1000>", thumb: "1000x1000>" },
    default_url: "/images/:style/missing.png"

end
