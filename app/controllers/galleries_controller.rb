class GalleriesController < ApplicationController

  def index
    @galleries = Gallery.all
    if params[:search]
        @galleries = Gallery.search(params[:search]).order("created_at DESC")
      else
        @galleries
    end
  end

   private

    def set_galleries_type
      @galleries_type = galleries_type_class.find(params[:id])
    end

    def galleries_type 
      Gossip.gallery_types.include?(params[:type]) ? params[:type] : "Gallery"
    end

    def galleries_type_class 
      galleries_type.constantize 
    end


end
