ActiveAdmin.register Event do

permit_params :title, :description, :event_guest,:event_host, :event_date, :event_address,
:city, :state, :country, :guest_image, :host_image

form do |f|
    f.inputs do
      f.input :title
      f.input :description
      f.input :guest_image, :as => :file
      f.input :host_image, :as => :file
      f.input :event_guest
      f.input :event_host
      f.input :event_date
      f.input :event_address
      f.input :city
      f.input :state
      f.input :country, :as => :string
    end
    f.actions
  end

  index do
    selectable_column
    column :title do | h |
      h.title
    end
    column :description do | h |
      h.description
    end
    column "Image" do |product|
      image_tag product.guest_image.url, :width =>  '180px',:height => '120px'
    end
    column "" do |resource|
      links =  link_to I18n.t('active_admin.view'), resource_path(resource), :class =>  "member_link view_link"
      links += link_to I18n.t('active_admin.edit'), edit_resource_path(resource), :class => "member_link edit_link"
      links += link_to I18n.t('active_admin.delete'), resource_path(resource), :method => :delete, :confirm => I18n.t('active_admin.delete_confirmation'), :class => "member_link delete_link"
      links += link_to I18n.t("Comments",'active_admin.comment').split('.')[-1], resource_path(resource), :class => "member_link comment_link"
      links
    end
  end

end
