ActiveAdmin.register News do

menu priority: 9, label: "News" 
permit_params :heading, :description, :author, :image, :video, :news_type,:category_id ,:on, :news
filter :id, as: :numeric, label: 'News ID'
filter :heading, as: :string, label: 'Filter Heading'
filter :description, as: :string, label: 'Filter Description'

form do |f|
  f.inputs do
    f.input :news_type, as: :select, :collection => News.news_type
    f.input :heading, :as => :ckeditor
    f.input :description, :as => :ckeditor
    f.input :image, :as => :file
    f.input :video, :as => :file
    f.input :author
    f.input :category_id, :input_html => { :value => 8 }, as: :hidden
  end
  f.actions
end

show do
    #attributes_table :name, :description, :owner, :company, :publish, :publisher
  active_admin_comments # Add this line for comment block
end 

index do
  selectable_column
  column :heading do | h |
    h.heading.html_safe
  end
  column :description do | h |
    h.description.html_safe
  end
  column "Image" do |product|
    image_tag product.image.url, :width =>  '180px',:height => '120px'
  end
  column "" do |resource|
    links =  link_to I18n.t('active_admin.view'), resource_path(resource), :class =>  "member_link view_link"
    links += link_to I18n.t('active_admin.edit'), edit_resource_path(resource), :class => "member_link edit_link"
    links += link_to I18n.t('active_admin.delete'), resource_path(resource), :method => :delete, :confirm => I18n.t('active_admin.delete_confirmation'), :class => "member_link delete_link"
    links += link_to I18n.t("Comments",'active_admin.comment').split('.')[-1], resource_path(resource), :class => "member_link comment_link"
    links
  end
end 

end
