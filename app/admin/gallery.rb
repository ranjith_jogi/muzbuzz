ActiveAdmin.register Gallery do

  menu priority: 5, label: "Gallery"
  permit_params :heading, :description, :author,:gallery_type, :video,:image,:category_id ,:on, :galleries
  filter :id, as: :numeric, label: 'Gallery ID'
  filter :heading, as: :string, label: 'Filter Heading'
  filter :description, as: :string, label: 'Filter Description'

  form do |f|
    f.inputs do
      f.input :gallery_type, as: :select, :collection => Gallery.gallery_types
      f.input :heading, :as => :ckeditor
      f.input :description, :as => :ckeditor
      f.input :image, :as => :file
      f.input :video, :as => :file
      f.input :author, :as => :ckeditor
      f.input :category_id, :input_html => { :value => 4 }, as: :hidden
    end
    f.actions
  end

  index do
    selectable_column
    column :heading do | h |
      h.heading.html_safe
    end
    column :description do | h |
      h.description.html_safe
    end
    column "Image" do |product|
      image_tag product.image.url, :width =>  '180px',:height => '120px'
    end
    column "" do |resource|
      links =  link_to I18n.t('active_admin.view'), resource_path(resource), :class =>  "member_link view_link"
      links += link_to I18n.t('active_admin.edit'), edit_resource_path(resource), :class => "member_link edit_link"
      links += link_to I18n.t('active_admin.delete'), resource_path(resource), :method => :delete, :confirm => I18n.t('active_admin.delete_confirmation'), :class => "member_link delete_link"
      links += link_to I18n.t("Comments",'active_admin.comment').split('.')[-1], resource_path(resource), :class => "member_link comment_link"
      links
    end
  end 

end
