ActiveAdmin.register BeautyAndSex do

menu priority: 1, label: "Beauty"
permit_params :tip_name, :tip_description, :tip_author, :beauty_and_sex_type, :image, :video, :tip_related_to, :category_id ,:on, :beauty_and_sexes
filter :id, as: :numeric, label: 'BeautyAndSex ID'
filter :tip_name, as: :string, label: 'Filter Heading'
filter :tip_description, as: :string, label: 'Filter Description'

form do |f|
  f.inputs do
    f.input :beauty_and_sex_type, as: :select, :collection => BeautyAndSex.types
    f.input :tip_related_to , as: :select, :collection =>['Health', 'Personal', 'Other']
    f.input :tip_name, :as => :ckeditor
    f.input :tip_description, :as => :ckeditor
    f.input :image, :as => :file
    f.input :video, :as => :file
    f.input :tip_author
    f.input :category_id, :input_html => { :value => 1 }, as: :hidden
  end
  f.actions
end

index do
  selectable_column
  column :tip_name do | h |
    h.tip_name.html_safe
  end
  column :tip_description do | h |
    h.tip_description.html_safe
  end
  column "Image" do |product|
    image_tag product.image.url, :width =>  '180px',:height => '120px'
  end
  column "" do |resource|
    links =  link_to I18n.t('active_admin.view'), resource_path(resource), :class =>  "member_link view_link"
    links += link_to I18n.t('active_admin.edit'), edit_resource_path(resource), :class => "member_link edit_link"
    links += link_to I18n.t('active_admin.delete'), resource_path(resource), :method => :delete, :confirm => I18n.t('active_admin.delete_confirmation'), :class => "member_link delete_link"
    links += link_to I18n.t("Comments",'active_admin.comment').split('.')[-1], resource_path(resource), :class => "member_link comment_link"    
    links
  end
end 


end
